# See: https://github.com/arduino/Arduino/wiki/Arduino-IDE-1.5-3rd-party-Hardware-specification
#  Eicon Boards
menu.pnum=Board part number

menu.xserial=U(S)ART support
menu.usb=USB support (if available)
menu.xusb=USB speed (if available)
menu.virtio=Virtual serial support

menu.opt=Optimize
menu.dbg=Debug symbols
menu.rtlib=C Runtime Library
menu.upload_method=Upload method


################################################################################
# F103C8T6
################################################################################
###  Arduino Menu --> Tools  --> Board:
DinRDuino.name=F103C8T6 BluePill  

DinRDuino.build.board=BluePill_HelloW
DinRDuino.build.core=arduino
DinRDuino.build.mcu=cortex-m3
DinRDuino.build.series=STM32F1xx
DinRDuino.build.cpu=F103C8T6
DinRDuino.build.vendor=Eicon
DinRDuino.build.kernel=lin
DinRDuino.build.variant=STM32F1xx/F103C8T6_BluePill
DinRDuino.build.variant_h=variant_{build.board}.h
DinRDuino.build.cmsis_lib_gcc=arm_cortexM3l_math
DinRDuino.build.extra_flags=-D{build.product_line} {build.enable_usb} {build.xSerial} {build.bootloader_flags}
DinRDuino.build.product_line=STM32F103xB
DinRDuino.upload.maximum_size=65536
DinRDuino.upload.maximum_data_size=20480

DinRDuino.menu.pnum.F103C8T6_BluePill=BluePill_HelloW
#DinRDuino.menu.pnum.FRED=BluePill_HelloW

# Upload menu
DinRDuino.menu.upload_method.jtagit-2-swd=openocd jtagit-2 swd
DinRDuino.menu.upload_method.jtagit-2-swd.upload.tool=openocd
DinRDuino.menu.upload_method.jtagit-2-swd.upload.protocol=swd
DinRDuino.menu.upload_method.jtagit-2-swd.upload.probe=jtagit-2

DinRDuino.menu.upload_method.jtagit-2-jtag=openocd jtagit-2 jtag
DinRDuino.menu.upload_method.jtagit-2-jtag.upload.tool=openocd
DinRDuino.menu.upload_method.jtagit-2-jtag.upload.protocol=jtag
DinRDuino.menu.upload_method.jtagit-2-jtag.upload.probe=jtagit-2

DinRDuino.menu.upload_method.openocd_debug=Jtagit-2 w/openocd+debug
DinRDuino.menu.upload_method.openocd_debug.upload.tool=openocd_debug
DinRDuino.menu.upload_method.openocd_debug.upload.protocol=0

DinRDuino.menu.upload_method.swdMethod=STM32CubeProgrammer (SWD)
DinRDuino.menu.upload_method.swdMethod.upload.protocol=0
DinRDuino.menu.upload_method.swdMethod.upload.options=-g
DinRDuino.menu.upload_method.swdMethod.upload.tool=stm32CubeProg

DinRDuino.menu.upload_method.serialMethod=STM32CubeProgrammer (Serial)
DinRDuino.menu.upload_method.serialMethod.upload.protocol=1
DinRDuino.menu.upload_method.serialMethod.upload.options={serial.port.file} -s
DinRDuino.menu.upload_method.serialMethod.upload.tool=stm32CubeProg

DinRDuino.menu.upload_method.dfuMethod=STM32CubeProgrammer (DFU)
DinRDuino.menu.upload_method.dfuMethod.upload.protocol=2
DinRDuino.menu.upload_method.dfuMethod.upload.options=-g
DinRDuino.menu.upload_method.dfuMethod.upload.tool=stm32CubeProg

DinRDuino.menu.upload_method.bmpMethod=BMP (Black Magic Probe)
DinRDuino.menu.upload_method.bmpMethod.upload.protocol=gdb_bmp
DinRDuino.menu.upload_method.bmpMethod.upload.tool=bmp_upload

DinRDuino.menu.upload_method.hidMethod=HID Bootloader 2.2
DinRDuino.menu.upload_method.hidMethod.upload.protocol=hid22
DinRDuino.menu.upload_method.hidMethod.upload.tool=hid_upload
DinRDuino.menu.upload_method.hidMethod.build.flash_offset=0x800
DinRDuino.menu.upload_method.hidMethod.build.bootloader_flags=-DBL_HID -DVECT_TAB_OFFSET={build.flash_offset}

DinRDuino.menu.upload_method.dfu2Method=Maple DFU Bootloader 2.0
DinRDuino.menu.upload_method.dfu2Method.upload.protocol=maple
DinRDuino.menu.upload_method.dfu2Method.upload.tool=maple_upload
DinRDuino.menu.upload_method.dfu2Method.upload.usbID=1EAF:0003
DinRDuino.menu.upload_method.dfu2Method.upload.altID=2
DinRDuino.menu.upload_method.dfu2Method.build.flash_offset=0x2000
DinRDuino.menu.upload_method.dfu2Method.build.bootloader_flags=-DBL_LEGACY_LEAF -DVECT_TAB_OFFSET={build.flash_offset}

DinRDuino.menu.upload_method.dfuoMethod=Maple DFU Bootloader original
DinRDuino.menu.upload_method.dfuoMethod.upload.protocol=maple
DinRDuino.menu.upload_method.dfuoMethod.upload.tool=maple_upload
DinRDuino.menu.upload_method.dfuoMethod.upload.usbID=1EAF:0003
DinRDuino.menu.upload_method.dfuoMethod.upload.altID=1
DinRDuino.menu.upload_method.dfuoMethod.build.flash_offset=0x5000
DinRDuino.menu.upload_method.dfuoMethod.build.bootloader_flags=-DBL_LEGACY_LEAF -DVECT_TAB_OFFSET={build.flash_offset}

# Serialx activation
DinRDuino.menu.xserial.generic=Enabled (generic 'Serial')
DinRDuino.menu.xserial.none=Enabled (no generic 'Serial')
DinRDuino.menu.xserial.none.build.xSerial=-DHAL_UART_MODULE_ENABLED -DHWSERIAL_NONE
DinRDuino.menu.xserial.disabled=Disabled (no Serial support)
DinRDuino.menu.xserial.disabled.build.xSerial=

# USB connectivity
DinRDuino.menu.usb.none=None
DinRDuino.menu.usb.CDCgen=CDC (generic 'Serial' supersede U(S)ART)
DinRDuino.menu.usb.CDCgen.build.enable_usb={build.usb_flags} -DUSBD_USE_CDC
DinRDuino.menu.usb.CDC=CDC (no generic 'Serial')
DinRDuino.menu.usb.CDC.build.enable_usb={build.usb_flags} -DUSBD_USE_CDC -DDISABLE_GENERIC_SERIALUSB
DinRDuino.menu.usb.HID=HID (keyboard and mouse)
DinRDuino.menu.usb.HID.build.enable_usb={build.usb_flags} -DUSBD_USE_HID_COMPOSITE
DinRDuino.menu.xusb.FS=Low/Full Speed
DinRDuino.menu.xusb.HS=High Speed
DinRDuino.menu.xusb.HS.build.usb_speed=-DUSE_USB_HS
DinRDuino.menu.xusb.HSFS=High Speed in Full Speed mode
DinRDuino.menu.xusb.HSFS.build.usb_speed=-DUSE_USB_HS -DUSE_USB_HS_IN_FS

# Optimizations
DinRDuino.menu.opt.osstd=Smallest (-Os default)
DinRDuino.menu.opt.oslto=Smallest (-Os) with LTO
DinRDuino.menu.opt.oslto.build.flags.optimize=-Os -flto
DinRDuino.menu.opt.o1std=Fast (-O1)
DinRDuino.menu.opt.o1std.build.flags.optimize=-O1
DinRDuino.menu.opt.o1lto=Fast (-O1) with LTO
DinRDuino.menu.opt.o1lto.build.flags.optimize=-O1 -flto
DinRDuino.menu.opt.o2std=Faster (-O2)
DinRDuino.menu.opt.o2std.build.flags.optimize=-O2
DinRDuino.menu.opt.o2lto=Faster (-O2) with LTO
DinRDuino.menu.opt.o2lto.build.flags.optimize=-O2 -flto
DinRDuino.menu.opt.o3std=Fastest (-O3)
DinRDuino.menu.opt.o3std.build.flags.optimize=-O3
DinRDuino.menu.opt.o3lto=Fastest (-O3) with LTO
DinRDuino.menu.opt.o3lto.build.flags.optimize=-O3 -flto
DinRDuino.menu.opt.ogstd=Debug (-g)
DinRDuino.menu.opt.ogstd.build.flags.optimize=-g -Og

# Debug information
DinRDuino.menu.dbg.none=None
DinRDuino.menu.dbg.enable=Enabled (-g)
DinRDuino.menu.dbg.enable.build.flags.debug=-g

# C Runtime Library
DinRDuino.menu.rtlib.nano=Newlib Nano (default)
DinRDuino.menu.rtlib.nanofp=Newlib Nano + Float Printf
DinRDuino.menu.rtlib.nanofp.build.flags.ldspecs=--specs=nano.specs -u _printf_float
DinRDuino.menu.rtlib.nanofs=Newlib Nano + Float Scanf
DinRDuino.menu.rtlib.nanofs.build.flags.ldspecs=--specs=nano.specs -u _scanf_float
DinRDuino.menu.rtlib.nanofps=Newlib Nano + Float Printf/Scanf
DinRDuino.menu.rtlib.nanofps.build.flags.ldspecs=--specs=nano.specs -u _printf_float -u _scanf_float
DinRDuino.menu.rtlib.full=Newlib Standard
DinRDuino.menu.rtlib.full.build.flags.ldspecs=


